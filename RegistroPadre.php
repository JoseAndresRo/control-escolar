﻿<?php
        require('conexion.php');
        $query = "SELECT Id_Grupo, Grupo FROM grupos";
        
        $resultado=$mysqli ->query($query);
        ?>

<!DOCTYPE html>

<html lang="es">

<head>
<title>Inicio sesión</title>
<meta charset="utf-8" />
<link href="bootstrap-4.3.1-dist/css/bootstrap.css" rel="stylesheet" />
<script src="validaciones.js"></script> 
  <script src="jquery-3.4.1.min.js"></script>
  
  <script type="text/javascript">
	$(document).ready(function(){
    //$('#lista1').val(0);
		recargarLista();

		$('#GrupoNino').change(function(){
			recargarLista();
		});
	})
</script>

<script type="text/javascript">
	function recargarLista(){
		$.ajax({
			type:"POST",
			url:"getNombreAlumno.php",
			data:"Dato=" + $('#GrupoNino').val(),
			success:function(r){
				$('#select2lista').html(r);
			}
		});
	}
</script>
   
      </head>

<body>
  <nav class="navbar navbar-expand-lg navbar-primary bg-info">
    <img src="imagenes/logo.png" class="rounded-circle "  alt="logo" style="width:50px;">
    
  </nav>
  <h2 class="text-center bg-warning" >Registro Padre de familia</h2>
  
  <div class="card-header  ">
        <div class="container-fluid  py-0 ">

            <div class="row col-form-label-lg  ">
                <div class="col-lg-7 col-md-7 col-xl-7 col-sm-7 mx-auto ">
                    <div class="card card-body  bg-light ">
                        
                        
  <form action="Registro.php" method="post"  novalidate onsubmit="return validar();" class="needs-validation">

  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom01">Nombre</label>
      <input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Nombre" required>
      <div class="valid-feedback">
        Campo lleno
      </div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Apellido Paterno</label>
      <input type="text" class="form-control" id="Apellido_Pa" name="Apellido_Pa" placeholder="Apellido" required>
      <div class="valid-feedback">
        Campo lleno
      </div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Apellido Materno</label>
      <input type="text" class="form-control" id="Apellido_Ma" name="Apellido_Ma" placeholder="Apellido Materno" required>
      <div class="valid-feedback">
        Campo lleno
      </div>
    </div>
    <div class="col-md-6   mb-3">
      <label for="validationCustom02 ">Dirección</label>
      <input type="Text" class="form-control" id="Direccion" name="Direccion" placeholder="Direccion" required>
      <div class="valid-feedback">
        Campo lleno
      </div>
    </div>
    <div class="col-md-3  mb-3">
      <label for="validationCustom02 ">N°Exterior</label>
      <input type="Number" class="form-control" id="NumExterior" name="NumExterior" placeholder="N°Exterior" required>
      <div class="valid-feedback">
        Campo lleno
      </div>
    </div>
    <div class="col-md-3   mb-3">
      <label for="validationCustom02 ">N°Interior</label>
      <input type="Number" class="form-control" id="NumInterior" name="NumInterior" placeholder="N°Interior" required>
      <div class="valid-feedback">
        Campo lleno
      </div>
    </div>
   <div class="row"> 
    <div class="col-md-4   mb-3">
      <label for="validationCustom02 ">Edad</label>
      <input type="number" class="form-control" id="Edad" name="Edad" placeholder="Edad" required>
      <div class="valid-feedback">
        Campo lleno
      </div>
    </div>
     <div class="input-group col-4 p-4 mb-3">
       
        <div class="input-group-prepend">
        
        <label class="input-group-text " for="inputGroupSelect01">Sexo</label></div>
        
        <select class="custom-select" id="Sexo" name="Sexo" required>
        <option selected></option>
        <option value="1">Masculino</option>
        <option value="2">Femenino</option>
        </select>
        <div class="valid-feedback offset-1">
            llenado
           </div>
        </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustomUsername">Correo Electronico</label>
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="">@</span>
        </div>
        <input type="text" class="form-control" id="Correo" name="Correo" placeholder="Correo Electronico" aria-describedby="inputGroupPrepend" required>
        <div class="valid-feedback">
         llenado
        </div>
      </div>
    </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="validationCustom03">Contraseña</label>
      <input type="Password" class="form-control" id="Contrasena" name="Contrasena" placeholder="Contraseña" required>
      <div class="valid-feedback">
       Campo lleno
      </div>
    </div>
    
    <div class="col-md-6 mb-3">
      <label for="validationCustom03">Confirmar Contraseña</label>
      <input type="Password" class="form-control" id="conficontrasena" name="conficontrasena" placeholder="Contraseña" required>
      <div class="valid-feedback">
       Campo lleno
      </div>
      
      </div>
      
      
    <div class="form-row">
    <div class="col-md-3 py-2 mb-3">
      <label for="validationCustom01">Telefono</label>
      <input type="text" class="form-control" id="Telefono" name="Telefono" placeholder="Telefono Celular" required>
      <div class="valid-feedback">
       Campo llenado
      </div>
    </div> 

      <div class="input-group col-6 py-5 mb-5">
       
        <div class="input-group-prepend">
          
        
        <label class="input-group-text " for="inputGroupSelect01">Parentesco del Alumno</label></div>
        <select class="custom-select" id="Parentesco" name="Parentesco" required>
        <option selected></option>
        <option value="Padre">Padre</option>
        <option value="Madre">Madre</option>
        <option value="Tutor">Tutor</option>
        </select>
        <div class="valid-feedback offset-1">
            llenado
           </div>
        </div>
        
        
     <h4 class="text-center">Información del Alumno</h4>

  
        <div class="input-group col- py-0 mb-5">
       <div class="input-group-prepend">
        <div>Seleccionar Grupo:<select id="GrupoNino" name="GrupoNino">
				<option value="0">Seleccionar Grupo</option>
				<?php while($row = $resultado->fetch_assoc()) { ?>
					<option value="<?php echo $row['Grupo']; ?>"><?php echo $row['Grupo']; ?></option>
				  <?php } ?>
			</select>
      </div>
      </div>
      </div>
      
      <div id="select2lista"></div>

          
  <button class="btn btn-primary offset-4 col-5" type="submit">Enviar</button>
  <a href="index.php" class="btn btn-dark  active" role="button" id="boton3">Regresar</a>
</form>
                        
                        

                        
                        
                        
                        
                    </div>    
                </div>
            </div>
      </div>
    </div>

    
    
    <script>
    (function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
    </script>

  <script src="bootstrap-4.3.1-dist/js/bootstrap.js"></script> 
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
</body>
</html>