<?php
require('conexion.php');
$query = "SELECT Id_Grupo, Grupo FROM grupos";

$resultado=$mysqli ->query($query);

   session_start();
$usuario = $_SESSION['username'];

if(!isset($usuario)){
  header("location: index.php");
}
?>
<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Registro de Alumnos</title>    
     <link href="bootstrap-4.3.1-dist/css/bootstrap.css" rel="stylesheet" />


     <script type="text/javascript">
	$(document).ready(function(){
    //$('#lista1').val(0);
		recargarLista();

		$('#GrupoNino').change(function(){
			recargarLista();
		});
	})
</script>

<script type="text/javascript">
	function recargarLista(){
		$.ajax({
			type:"POST",
			url:"getIdGrupo.php",
			data:"Dato=" + $('#GrupoNino').val(),
			success:function(r){
				$('#select2lista').html(r);
			}
		});
	}
</script>

  </head>  
  <body>    
   <nav class="navbar navbar-expand-lg navbar-primary bg-info">
    <img src="imagenes/logo.png" class="rounded-circle "  alt="logo" style="width:50px;">
    <a href="CerrarSesion.php" class="btn btn-success  active" role="button">Cerrar Sesion</a>
    
  </nav>
  <h4 class="text-center"> Registrar Alumnos </h4>
    <div class="card-header  ">
        <div class="container-fluid  py-5 " >

            <div class="row col-form-label-lg  ">
                <div class="col-lg-7 col-md-7 col-xl-7 col-sm-7 mx-auto ">
                    <div class="card card-body  bg-light ">
    <form action="BackReAlumnos.php" method="post" class="needs-validation" novalidate >
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom01">Nombre del alumno</label>
      <input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Nombre"  required>
      <div class="valid-feedback">
        Correcto
      </div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Boleta</label>
      <input type="text" class="form-control" id="Boleta" name="Boleta" placeholder="Boleta"  required>
      <div class="valid-feedback">
        Correcto
      </div>
    </div>

    

    <div class="input-group col-4 py-4 mb-3">
       
       <div class="input-group-prepend">
       <div class="input-group col- py-0 mb-5">
      <div class="input-group-prepend">
       <div>Seleccionar Grupo:<select id="GrupoNino" name="GrupoNino">
       <option value="0">Seleccionar Grupo</option>
       <?php while($row = $resultado->fetch_assoc()) { ?>
         <option value="<?php echo $row['Grupo']; ?>"><?php echo $row['Grupo']; ?></option>
         <?php } ?>
     </select>
     </div>
     </div>
     </div>
       

       </select>
       </div>
 </div>
 <div id="select2lista"></div>

      
       
        <button class="btn btn-primary offset-4 col-5" type="submit">Guardar</button>
        <a href="Alumnos.php" class="btn btn-dark  offset-4 col-5 active" role="button" id="boton1">Regresar</a>
        </div>
        </div>
      </div>
      </div>
      </div>     
  </form>
    <script>
    (function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="bootstrap-4.3.1-dist/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  </body>  
</html>