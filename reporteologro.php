<?php
        require('conexion.php');
        $query = "SELECT Id_Grupo, Grupo FROM grupos";
        
        $resultado=$mysqli ->query($query);

   session_start();
$usuario = $_SESSION['username'];

if(!isset($usuario)){
  header("location: index.php");
}
        ?>

<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Registro Reportes</title>    
     <link href="bootstrap-4.3.1-dist/css/bootstrap.css" rel="stylesheet" />
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
     <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
     <script src="jquery-3.4.1.min.js"></script>

     <script type="text/javascript">
	$(document).ready(function(){
    //$('#lista1').val(0);
		recargarLista();

		$('#GrupoNino').change(function(){
			recargarLista();
		});
	})
</script>

<script type="text/javascript">
	function recargarLista(){
		$.ajax({
			type:"POST",
			url:"getNombreAlumno.php",
			data:"Dato=" + $('#GrupoNino').val(),
			success:function(r){
				$('#select2lista').html(r);
			}
		});
	}
</script>
  </head>  
  <body>    
   <nav class="navbar navbar-expand-lg navbar-primary bg-info">
    
    
    <a href="CerrarSesion.php" class="btn btn-success  active" role="button">Cerrar Sesion</a>
    
  
  <div class="continer col-lg-0 offset-lg-10">
  <img src="imagenes/logo.png" class="rounded-circle "  alt="logo" style="width:50px;">
  </div>
  
  
</nav>
<h2 class="text-center bg-warning" >Generacion de Reportes</h2>
      
        <div class="container-fluid  py-5 " >
        

            <div class="row col-form-label-lg  ">
                <div class="col-lg-7 col-md-7 col-xl-7 col-sm-7 mx-auto ">
                    <div class="card card-body  bg-light ">
                    
                    <nav aria-label="breadcrumb">

             
                   <form action="RegistroReporte.php" method="post" class="needs-validation" novalidate >

                   <div class="input-group col- py-0 mb-5">
       <div class="input-group-prepend">
        <div>Seleccionar Grupo:<select id="GrupoNino" name="GrupoNino">
				<option value="0">Seleccionar Grupo</option>
				<?php while($row = $resultado->fetch_assoc()) { ?>
					<option value="<?php echo $row['Grupo']; ?>"><?php echo $row['Grupo']; ?></option>
				  <?php } ?>
			</select>
      </div>
      </div>
      </div>   
      <div id="select2lista"></div>   

        <!--<div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom01">Nombre del alumno</label>
      <input type="text" class="form-control" id="select2lista" name="select2lista" >
      <div class="valid-feedback">
        Looks good!
      </div>
        -->

      
    <div class="form-row">
    <div class="input-group col-7 py- mb-3">
       <label for="validationCustom01">Reporte/Logro</label>
        <div class="input-group-prepend ">
        
        <label class="input-group-text " for="inputGroupSelect01">Tipo de Registro </label></div>
        <select class="custom-select" id="Reporte" name="Reporte">
        <option selected></option>
        <option value="Reporte">Reporte</option>
        <option value="Logro">Logro</option>
        </select>
        </div>
      </div>
      
      <div class="col-sm-6">
        <div class="form-group">
        <label class="input-group-text " for="inputGroupSelect01">Fecha
            <div class="input-group date" id="datetimepicker12" name="datetimepicker12" data-target-input="nearest">
                <input data-toggle="datetimepicker" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker12" id="datetimepicker12" name="datetimepicker12"/>
                <div class="input-group-append" data-target="#datetimepicker12" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </label>
                </div>
            </div>
        </div>
    </div>
     
      <div class="container">

    <div class="container">
  
  <form >
    <div class="form-group">
      <label for="comment">Describa el hecho:</label>
      <textarea class="form-control" rows="5" id="Comentario" name="Comentario"></textarea>
    </div>
  </form>
</div>
        
  <button class="btn btn-primary offset-4 col-5" type="submit">Enviar</button>
</form>
                        
                        

                        
                        
                        
                        
                    </div>    
                </div>
            </div>
      </div>
    </div>
                    
    
    
    
    
    
    
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker12').datetimepicker({
              format: 'L',
                daysOfWeekDisabled: [0, 6]
            });
        });
    </script>
</div>
    
    
    
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>


    <script src="bootstrap-4.3.1-dist/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  </body>  
</html>