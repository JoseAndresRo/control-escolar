<?php
   session_start();
$usuario = $_SESSION['username'];

if(!isset($usuario)){
  header("location: index.php");
}
include_once('conectaProfesores.php');
$consulta= laconsulta();
?>
<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Eliminar Profesores</title>    
     <link href="bootstrap-4.3.1-dist/css/bootstrap.css" rel="stylesheet" />




  </head>  
  <body>    
   <nav class="navbar navbar-expand-lg navbar-primary bg-info">
    <img src="imagenes/logo.png" class="rounded-circle "  alt="logo" style="width:50px;">
    <a href="CerrarSesion.php" class="btn btn-success  active" role="button">Cerrar Sesion</a>
  </nav>

<div class="row">

<div class="col-md-12 ">
<h2 class="text-center bg-warning" >Elimar Profesores</h2>
  <table class="table table-striped">
    <head>
      <tr>
        <th width="100">Matricula</th>
        <th width="250">Nombre</th>
        <th width="200">Paterno</th>
        <th width="200">Materno</th>
        <th width="200">Grupo</th>
        <th width="200">Usuario</th>
        <th width="350">Accción
</tr>
</head>
<tbody>
  <?php
  while($persona=$consulta->fetch_assoc())
  {
    ?>
    <tr>
      <td><?php echo $persona['Matricula']; ?></td>
      <td><?php echo $persona['NombreProfesor']; ?></td>
      <td><?php echo $persona['Apellido_Pa']; ?></td>
      <td><?php echo $persona['Apellido_Ma']; ?></td>
      <td><?php echo $persona['Grupo']; ?></td>
      <td><?php echo $persona['Correo']; ?></td>
      <td>
        <a href="#" class="btn btn-warning" onclick="preguntar(<?php echo $persona['Matricula']; ?>)">
        Eliminar</a>
  </td>
  </tr>
  <?php
  }
  ?>
  </tbody>
</table>
</div>
</div>
<footer class="row">
  <div class="col-md-12">
    
</div>
</footer>
</div>



<a href="Profesores.php" class="btn btn-dark  offset-4 col-5 active" role="button" id="boton1">Regresar</a>

<script type="text/javascript">
//Borrar con alert
function preguntar(id){
  if(confirm('¿Estas seguro que deseas eliminar la matricula ' + id + ' ?'))
  {
    window.location.href="ConexionBorrar.php?id="+ id;
  }
}
</script>


<!--
  <h2 class="text-center bg-warning" >Elimar Profesores</h2>
    <div class="card-header  ">
        <div class="container-fluid  py-5 " >

            <div class="row col-form-label-lg  ">
                <div class="col-lg-7 col-md-7 col-xl-7 col-sm-7 mx-auto ">
                    <div class="card card-body  bg-light ">
    <form action="RegistroProfesor.php" method="post" class="needs-validation" novalidate >


  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom01">Matricula del profesor</label>
      <input type="text" class="form-control" id="Id_Grupo" name="Id_Grupo" placeholder="ID"  required>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Nombre</label>
      <input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Grupos"  required>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Apellido Paterno</label>
      <input type="text" class="form-control" id="ApellidoPa" name="ApellidoPa" placeholder="Grupos"  required>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Apellido Materno</label>
      <input type="text" class="form-control" id="ApellidoMa" name="ApellidoMa" placeholder="Grupos"  required>
    </div>

        <button class="btn btn-primary offset-4 col-5" type="submit">Eliminar</button>
        <a href="Profesores.php" class="btn btn-dark  offset-4 col-5 active" role="button" id="boton1">Regresar</a>
        </div>
        </div>
      </div>
      </div>
      </div>     
  </form>
-->
    <script>
    (function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="bootstrap-4.3.1-dist/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  </body>  
</html>