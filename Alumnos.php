<?php
session_start();
$usuario = $_SESSION['username'];

if(!isset($usuario)){
  header("location: index.php");
}

?>

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <title>Alumnos</title>

    <link href="bootstrap-4.3.1-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
</head>

<body>   
  <nav class="navbar navbar-expand-lg navbar-primary bg-info">
    <div class="dropdown ">
  <button class="btn btn-info dropdown-toggle " type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Menu
  </button>
  <a href="CerrarSesion.php" class="btn btn-success  active" role="button">Cerrar Sesion</a>
  <div class="dropdown-menu " aria-labelledby="dropdownMenu2">
    <a class="dropdown-item" href="index.html">Inicio</a>
    <a class="dropdown-item" href="Contacto.html">Contacto</a>  
  </div>
</div>
  </nav>


  <div class="card-header  ">
    <h3 class="offset-4"> Bienvenido al apartado de Alumnos </h3>
    <div class="container-fluid  py-5 ">

        <div class="row col-form-label-lg  ">
            <div class="col-lg-7 col-md-7 col-xl-7 col-sm-7 mx-auto ">
                <div class="card card-body  bg-light ">

                <a href="RegistroAlumnos.php" class="btn btn-dark  active" role="button" id="boton1">Agregar Alumnos</a>
               <!-- <a href="EditarAlumnos.php" class="btn btn-dark  active" role="button" id="boton2">Editar Alumnos</a>-->
                <a href="EliminarAlumnos.php" class="btn btn-dark  active" role="button" id="boton3">Eliminar Registros de Alumnos</a>

                </div>
            </div>
        </div>
    </div>
  </div>
  <a href="Admon.php" class="btn btn-dark  offset-4 col-4 active" role="button" id="boton1">Regresar</a>
    
   
  




    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

</body>
</html>