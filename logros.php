<?php
session_start();
$usuario = $_SESSION['username'];

if(!isset($usuario)){
  header("location: index.php");
}

include_once('ChecarReportes.php');
$consulta= laconsulta();

?>
<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Registro Profesor</title>    
     <link href="bootstrap-4.3.1-dist/css/bootstrap.css" rel="stylesheet" />
  </head>  
  <body>    
   <nav class="navbar navbar-expand-lg navbar-primary bg-info">
    
    
    <a href="CerrarSesion.php" class="btn btn-success  active" role="button">Cerrar Sesion</a>
    
  
  <div class="continer col-lg-0 offset-lg-10">
  <img src="imagenes/logo.png" class="rounded-circle "  alt="logo" style="width:50px;">
  </div>
  </nav>
  <h2 class="text-center bg-success">Reportes/Logros del Alumno</h2>
<div class="card-header  ">
          
</nav>

<div class="row">

<div class="col-md-12 ">

  <table class="table table-striped">
    <head>
      <tr>
        <th width="300">Nombre Alumno</th>
        <th width="250">Grupo</th>
        <th width="250">Fecha</th>
        <th width="250">Tipo de Reporte</th>
        <th width="250">Descripción de reporte</th>
        
</tr>
</head>
<tbody>
  <?php
  while($persona=$consulta->fetch_assoc())
  {
    ?>
    <tr>
      <td><?php echo $persona['NombreAlumno']; ?></td>
      <td><?php echo $persona['Grupo']; ?></td>
      <td><?php echo $persona['Fecha']; ?></td>
      <td><?php echo $persona['TipoReporte']; ?></td>
      <td><?php echo $persona['Reporte']; ?></td>
      <td>
  </td>
  </tr>
  <?php
  }
  ?>
  </tbody>
</table>
</div>
</div>
         
        
        <div class="container-fluid  py-5 " >

            <div class="row col-form-label-lg  ">
                <div class="col-lg-10 col-md-10 col-xl-10 col-sm-10 mx-auto ">
                    <div class="card card-body  bg-light ">
                    
                    <nav aria-label="breadcrumb">
             
         
<table class="table table-bordered table-success">
  <thead>
    <tr>
      <th scope="col">Fecha</th>
      <th scope="col">Logro</th>
     
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
     
    </tr>
    
   
  </tbody>
</table>
                        
                        
                        
                        
                    </div>    
                </div>
            </div>
      </div>
    </div>
                    
    
    
    
    
    
    
    
    
    
    
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="bootstrap-4.3.1-dist/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  </body>  
</html>